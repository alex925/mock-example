import asyncio
from pathlib import Path

import aiofiles
import aiohttp
from requests import get as request_get

from mock_example.config import conf

AVATAR_URL = 'https://cdn.pixabay.com/photo/2022/04/21/19/47/lion-7148207_1280.jpg'


def simple_downloader(url: str, path: Path) -> bytes:
    resp = request_get(url)
    path.write_bytes(resp.content)
    return resp.content


class AsyncImgDownloader:
    def __init__(self, url: str, path: Path):
        self.url = url
        self.path = path
        self.content = None

    async def download(self):
        self.content = await self._fetch()
        await self._save(self.content)

    async def _fetch(self) -> bytes:
        async with aiohttp.ClientSession() as session:
            async with session.get(self.url) as resp:
                return await resp.read()

    async def _save(self, data: bytes):
        out_file = await aiofiles.open(self.path, mode='wb')
        await out_file.write(data)
        await out_file.close()


def download_avatar(path: Path) -> bytes:
    if conf.DOWNLOADER_VERSION != 'async':
        avatar_content = simple_downloader(url=AVATAR_URL, path=path)
    else:
        async_downloader = AsyncImgDownloader(url=AVATAR_URL, path=path)
        asyncio.run(async_downloader.download())
        avatar_content = async_downloader.content

    return avatar_content
