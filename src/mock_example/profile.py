import datetime
import random


def get_profile_info():
    return {
        'name': 'Alex',
        'email': 'alex@gmail.com',
        'current_date': datetime.datetime.now()
    }


def get_friends():
    friends = ['dima', 'vika', 'sasha']
    random.shuffle(friends)
    for name in friends:
        yield name


async def async_get_friends():
    friends = ['dima', 'vika', 'sasha']
    random.shuffle(friends)
    for name in friends:
        yield name
