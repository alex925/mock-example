from mock_example.config import conf
from mock_example.profile import get_profile_info
from mock_example.downloader import download_avatar


def get_all_info():
    avatar = download_avatar(conf.AVATAR_PATH)
    profile = get_profile_info()
    profile['avatar'] = avatar
    return profile


if __name__ == '__main__':
    print(get_all_info())
