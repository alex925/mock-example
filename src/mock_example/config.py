from pathlib import Path


class Config:
    def __init__(self):
        self.BASE_DIR = Path(__name__).absolute().parent.parent.parent

        self.MEDIA_DIR = self.BASE_DIR / 'media'
        self.MEDIA_DIR.mkdir(parents=True, exist_ok=True)

        self.AVATAR_PATH = self.MEDIA_DIR / 'avatar.jpg'

        self.DOWNLOADER_VERSION = 'async'


conf = Config()
