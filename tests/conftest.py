import tempfile
from pathlib import Path

import pytest


@pytest.fixture
def tmp_dir():
    return Path(tempfile.mkdtemp())
