from unittest.mock import ANY

from pytest_mock import MockerFixture

import mock_example.profile as profile
from mock_example.profile import get_profile_info


def test_get_profile_info():
    # act
    info = get_profile_info()

    # assert
    assert info == {
        'name': 'Alex',
        'email': 'alex@gmail.com',
        'current_date': ANY  # ANY очень удобно использовать для данных, которые могут постоянно меняться
    }


def test_get_friends(mocker: MockerFixture):
    # arrange
    expected_friends = ['alex', 'alex']
    # Пример как можно легко подменить результат работы генератора
    mocker.patch('mock_example.profile.get_friends', side_effect=[expected_friends])

    # act
    friends = [i for i in profile.get_friends()]

    # assert
    assert friends == expected_friends


async def test_async_get_friends(mocker: MockerFixture):
    # arrange
    expected_friends = ['alex', 'alex']

    # Пример как можно легко подменить результат работы асинхронного генератора
    iterator_mock = mocker.MagicMock()
    iterator_mock.__aiter__.return_value = expected_friends
    mocker.patch('mock_example.profile.async_get_friends', return_value=iterator_mock)

    # act
    friends = [i async for i in profile.async_get_friends()]

    # assert
    assert friends == expected_friends
