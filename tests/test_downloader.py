from pathlib import Path
from unittest import mock

import pytest

from mock_example.downloader import download_avatar, AVATAR_URL, AsyncImgDownloader
from mock_example.config import conf


@mock.patch('mock_example.downloader.request_get')
def test_download_avatar__patch_download_func__version_1(
        get_mock,
        avatar_path,
        avatar_content,
        sync_downloader_version_mock
):
    # arrange
    # Вызываем мок подменяемой функции, чтобы получить объект эмулирующий ответ от сервера и объявляем в нем атрибуты,
    # нужные для работы проверяемой нами функции (download_avatar).
    response = get_mock()
    response.content = avatar_content

    # act
    avatar = download_avatar(avatar_path)

    # assert
    get_mock.assert_called_with(AVATAR_URL)
    assert avatar == avatar_content
    assert avatar_path.exists()
    assert avatar_path.read_bytes() == avatar_content


@mock.patch('mock_example.downloader.request_get')
def test_download_avatar__patch_download_func__version_2(
        get_mock,
        avatar_path,
        avatar_content,
        sync_downloader_version_mock
):
    # arrange
    # Создаем мок эмулирующий ответ от сервера и вставляем в него нужные нам данные, которые мы якобы получили в
    # результате запроса
    response = mock.Mock()
    response.content = avatar_content
    # Говорим моку, который прикидывается функцией requests.get, что при каждом его вызове он должен возвращать наш
    # фейковый ответ от сервера.
    get_mock.return_value = response

    # act
    avatar = download_avatar(avatar_path)

    # assert
    get_mock.assert_called_with(AVATAR_URL)
    assert avatar == avatar_content
    assert avatar_path.exists()
    assert avatar_path.read_bytes() == avatar_content


def test_download_avatar__replacement_download_func(
        avatar_path,
        avatar_content,
        request_get_mock,
        sync_downloader_version_mock
):
    # act
    avatar = download_avatar(avatar_path)

    # assert
    assert avatar == avatar_content
    assert avatar_path.exists()
    assert avatar_path.read_bytes() == avatar_content


@mock.patch('mock_example.downloader.AsyncImgDownloader._fetch')
def test_download_avatar__use_async_downloader(
        fetch_mock,
        avatar_path,
        avatar_content,
        async_downloader_version_mock,
):
    # arrange
    fetch_mock.return_value = avatar_content

    # act
    download_avatar(avatar_path)

    # assert
    assert avatar_path.exists()
    assert avatar_path.read_bytes() == avatar_content


class TestAsyncImgDownloader:
    @mock.patch('mock_example.downloader.AsyncImgDownloader._fetch')
    async def test_download(self, fetch_mock, avatar_path, avatar_content):
        # arrange
        fetch_mock.return_value = avatar_content

        # act
        inst = AsyncImgDownloader(url=AVATAR_URL, path=avatar_path)
        await inst.download()

        # assert
        assert inst.content == avatar_content


@pytest.fixture
def avatar_path(tmp_dir) -> Path:
    return tmp_dir / 'avatar.jpg'


@pytest.fixture
def avatar_content():
    return b'12345'


@pytest.yield_fixture(name='request_get_mock')
def request_get_mock_(avatar_content):
    def stub(*args, **kwargs):
        resp_mock = mock.Mock()
        resp_mock.content = avatar_content
        return resp_mock

    # Так как функция get импортировалась через from, то нужно указывать именно такой путь до нее. Это связано с тем,
    # что при использовании from, импортируемый объект копируется в область видимости импортируемого модуля.

    # Параметр new позволяет подменить функцию requests.get нашей собственной реализацией, сделанной нами ранее.
    with mock.patch('mock_example.downloader.request_get', new=stub):
        yield


@pytest.yield_fixture(name='sync_downloader_version_mock')
def sync_downloader_version_mock_():
    # Подменяем значение в конфиге, который используется в программе
    with mock.patch.object(conf, 'DOWNLOADER_VERSION', 'sync') as conf_downloader_version:
        yield


@pytest.yield_fixture(name='async_downloader_version_mock')
def async_downloader_version_mock_():
    # Подменяем значение в конфиге, который используется в программе
    with mock.patch.object(conf, 'DOWNLOADER_VERSION', 'async') as conf_downloader_version:
        yield
